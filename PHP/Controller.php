<?php
	include_once "Order/Order.php";
	include_once "Store/Store.php";

	$content = trim(file_get_contents("php://input"));
	$decoded = json_decode($content, true);

	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
	header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
	header('Content-Type: application/json');

	try {
		$invoiceDelivery = GetInvoiceDelivery($decoded);

		http_response_code(200);
		echo(json_encode($invoiceDelivery));
	} catch (Exception $e) {
		http_response_code(501);
		echo(json_encode($e->getMessage()));
	}

	function GetInvoiceDelivery($decoded) {
		if(!is_array($decoded)){
			throw new Exception('Received content contained invalid JSON!');
		}

		$order = new Order($decoded["weight"], $decoded["country"]);
		$store = new Store($order);
		return $store->GenerateInvoiceDelivery();
	}
?>