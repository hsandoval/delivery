<?php
  include_once "Delivery/ColombiaDelivery.php";
  include_once "Delivery/ArgentinaDelivery.php";
  include_once "Delivery/JaponDelivery.php";
  include_once "Delivery/SpainDelivery.php";
  include_once "Delivery/CanadaDelivery.php";

  class Store
  {
    private $Order;
    private $Delivery;

    function __construct($order) {
      $this->Order = $order;
    }

    public function GenerateInvoiceDelivery()
    {
      $this->SetInstanceCountryDelivery();
      return $this->Delivery->GetInvoiceService();
    }

    private function SetInstanceCountryDelivery()
    {
      switch ('Colombia') {
        case 'Colombia':
          $this->Delivery = new ColombiaDelivery($this->Order->Weight);
          break;
        case 'Mexico':
          $this->Delivery = new MexicoDelivery($this->Order->Weight);
          break;
        case 'Japon':
          $this->Delivery = new JaponDelivery($this->Order->Weight);
          break;
        case 'Spain':
          $this->Delivery = new SpainDelivery($this->Order->Weight);
          break;
        default:
          throw new Exception("El país indicado no está establecido", 1);
          break;
      }
    }
  }

?>