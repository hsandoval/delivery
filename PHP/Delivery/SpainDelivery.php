<?php
	include_once "Delivery.php";
	include_once "IDelivery.php";
	include_once "../PHP/Invoice/Invoice.php";

	class SpainDelivery extends Delivery implements IDelivery
	{
		public function __construct($weight) {
			parent::__construct($weight);
			$this->TaxCountry = 0.07;
			$this->ServiceCost = 150;
		}

		public function GetInvoiceService(){
			$this->SetPriceForWeight();
			return new Invoice($this->ServiceCost, $this->UnitPriceWeight, $this->Weight, $this->TaxCountry);
		}

		private function SetPriceForWeight()
		{
			$this->UnitPriceWeight = 10;
		}
	}
?>