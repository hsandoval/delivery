<?php
	include_once "Delivery.php";
	include_once "IDelivery.php";
	include_once "../PHP/Invoice/Invoice.php";

	class CanadaDelivery extends Delivery implements IDelivery
	{
		public function __construct($weight) {
			parent::__construct($weight);
			$this->TaxCountry = 0;
			$this->ServiceCost = 150;
		}

		public function GetInvoiceService(){
			$this->SetPriceForWeight();
			return new Invoice($this->ServiceCost, $this->UnitPriceWeight, $this->Weight, $this->TaxCountry);
		}

		private function SetPriceForWeight()
		{
			if ($this->Weight <= 4) {
				$this->UnitPriceWeight = 25;
			} else {
				$this->UnitPriceWeight = 20;
			}
		}
	}
?>