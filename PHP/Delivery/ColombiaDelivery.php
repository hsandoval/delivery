<?php
	include_once "Delivery.php";
	include_once "IDelivery.php";
	include_once "../PHP/Invoice/Invoice.php";

	class ColombiaDelivery extends Delivery implements IDelivery
	{
		public function __construct($weight) {
			parent::__construct($weight);
			$this->TaxCountry = 0.12;
			$this->ServiceCost = 2000;
		}

		public function GetInvoiceService(){
			$this->SetPriceForWeight();
			return new Invoice($this->ServiceCost, $this->UnitPriceWeight, $this->Weight, $this->TaxCountry);
		}

		private function SetPriceForWeight()
		{
			if ($this->Weight <= 2) {
				$this->UnitPriceWeight = 750;
			} else {
				$this->UnitPriceWeight = 500;
			}
		}
	}
?>