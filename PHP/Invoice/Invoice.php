<?php
  class Invoice
  {
    public $ServiceCost;
    public $UnitPriceWeight;
    public $Weight;
    public $TotalPriceForWeight;
    public $SubTotal;
    public $PercentTax;
    public $TotalTax;
    public $Total;

    public function __construct($ServiceCost, $UnitPriceWeight, $Weight, $PercentTax) {
      $this->ServiceCost = $ServiceCost;
      $this->UnitPriceWeight = $UnitPriceWeight;
      $this->Weight = $Weight;
      $this->PercentTax = $PercentTax;
      $this->TotalPriceForWeight = $this->GetTotalPriceForWeight();
      $this->SubTotal = $this->GetSubTotal();
      $this->TotalTax = $this->GetTotalTax();
      $this->Total = $this->GetTotal();
    }

    private function GetTotalPriceForWeight()
    {
      return $this->UnitPriceWeight * $this->Weight;
    }

    private function GetSubTotal()
    {
      return $this->TotalPriceForWeight + $this->ServiceCost;
    }

    private function GetTotalTax()
    {
      return $this->SubTotal * $this->PercentTax;
    }

    private function GetTotal()
    {
      return $this->SubTotal + $this->TotalTax;
    }
  }
?>