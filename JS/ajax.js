const calculateCost = async (event) => {
	event.preventDefault();
	const form = document.getElementById('frmDelivery');
	let jsonData = {};

	for (const pair of new FormData(form)) {
		jsonData[pair[0]] = pair[1];
	}

	const response = await sendRequest('controller.php', jsonData);
	await setValuesInvoice(response.body);
}

const setValuesInvoice = async (values) => {
	await Promise.all([
		setValuesElementByName('costService', values.ServiceCost),
		setValuesElementByName('unitPriceWeight', values.UnitPriceWeight),
		setValuesElementByName('totalPriceForWeight', values.TotalPriceForWeight),
		setValuesElementByName('totalWeight', values.Weight),
		setValuesElementByName('subTotal', values.SubTotal),
		setValuesElementByName('percentTax', values.PercentTax),
		setValuesElementByName('totalTax', values.TotalTax),
		setValuesElementByName('total', values.Total),
	]);
}

const sendRequest = (action, jsonBody) => {
	const URL = `${document.location}PHP/${action}`;
	return new Promise(async (resolve, reject) => {
		await fetch(URL, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(jsonBody)
		})
			.then(response => response.text().then(data => {
				return ({ status: response.status, body: JSON.parse(data) })
			}
			))
			.then((data) => resolve(data))
			.catch(err => reject(Error(err.message)));
	});
};

const setValuesElementById = async (idElement, value) =>
	document.getElementById(idElement).value = value;

const setValuesElementByName = async (idElement, value) =>
	document.getElementsByName(idElement).forEach(element => element.value = value)